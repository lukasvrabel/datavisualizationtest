#!/usr/bin/env python
#coding: utf8 

import requests
from bs4 import BeautifulStoneSoup

def get_url_for_country_from(countryCodeFrom = 'TOT'):
    params = {
          'base': 'http://stats.oecd.org/restsdmx/sdmx.ashx/GetData/MIG/'
        , 'countryFrom': countryCodeFrom
        , 'dataType': 'B14.TOT'
        , 'countries': 'AUS+AUT+BEL+CAN+CHL+CZE+DNK+EST+FIN+FRA+DEU+GRC+HUN+ISL+IRL+ISR+ITA+JPN+KOR+LUX+MEX+NLD+NZL+NOR+POL+PRT+SVK+SVN+ESP+SWE+CHE+TUR+GBR+USA'
        , 'startTime': '2012'
        , 'endTime': '2012'
    }
    url = "{base}{countryFrom}.{dataType}.{countries}/all?startTime={startTime}&endTime={endTime}".format(**params)
    return url
    
    

def get_counts_from_xml(xml):
    soup = BeautifulStoneSoup(xml)
    series = soup.DataSet.findAll('Series')
    
    counts = []
    for s in series:
        counts.append({
              'countryCodeFrom': s.SeriesKey.findAll('Value', {'concept': 'CO2'})[0]['value']
            , 'countryCodeIn': s.SeriesKey.findAll('Value', {'concept': 'COU'})[0]['value']
            , 'count':  float( s.Obs.ObsValue['value'] )
        })
    return counts

        
        
def get_counts(countryCode):
    url = get_url_for_country_from(countryCode)
    r = requests.get(url)
    xml = r.text
    countries = get_counts_from_xml(xml)
    return countries
    

    

if __name__ == '__main__':
    print 'run nosetests'
    
    