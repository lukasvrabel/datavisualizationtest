#!/usr/bin/env python


from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker
from models import *

default_params = {
      'host': '0.0.0.0'
    , 'user': 'lukasvrabel'
    , 'passwd': ''
    , 'db': 'c9'
    , 'dialect': 'mysql'
}


class SqlAlchemyDB:
    session = None
    country_manager = None
    foreign_born_manager = None
    engine = None

    def __init__(
              self
            , host = default_params['host']
            , user = default_params['user']
            , passwd = default_params['passwd']
            , db = default_params['db']
            , dialect = default_params['dialect']
        ):
        
        url = '{dialect}://{user}:{passwd}@{host}/{db}'.format(
              dialect = dialect
            , user = user
            , passwd = passwd
            , host = host
            , db = db
        )
        self.engine = create_engine(url)
    
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        
        
    def getCountryManager(self):
        if not self.country_manager: 
            self.country_manager = CountryManager(self.session)
        return self.country_manager
    
    def getForeignBornManager(self):
        if not self.foreign_born_manager:
            self.foreign_born_manager = ForeignBornManager(self.session)
            
        return self.foreign_born_manager
        
    def init_test_data(self):
        Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)
        self._add_test_data()

    def _add_test_data(self):
        countries = [
              Country(code="AUS", name="Australia")
            , Country(code="SVK", name="Slovakia")
            , Country(code="USA", name="United States")
        ]
        for c in countries:
            self.session.add(c)
        
        foreign_borns = [
              ForeignBorn(codeFrom="AUS", codeIn="USA", count=100)
            , ForeignBorn(codeFrom="USA", codeIn="AUS", count=200)
            , ForeignBorn(codeFrom="USA", codeIn="SVK", count=300)
        ]
        for fb in foreign_borns:
            self.session.add(fb)
            
        self.session.commit()
        
        

class CountryManager:
    session = None
    
    def __init__(self, session):
        self.session = session    
        
    def getAll(self):
        return self.session.query(Country).all()
    
    def getByCode(self, code):
        return self.session.query(Country).filter(Country.code == code).one()

    
    
class ForeignBornManager:
    session = None
    
    def __init__(self, session):
        self.session = session    
    
    def getAll(self):
        return self.session.query(ForeignBorn).all()
        
    
    def getByCountryFrom(self, countryFrom):
        fbs = self.session.query(ForeignBorn)
        if isinstance(countryFrom, basestring):
            return fbs.filter(ForeignBorn.codeFrom == countryFrom).all()
        elif isinstance(countryFrom, Country):
            return fbs.filter(ForeignBorn.countryFrom == countryFrom).all()
        else:
            raise TypeError("countryFrom must be Country or String")
        
    def getByCountryIn(self, countryIn):
        fbs = self.session.query(ForeignBorn)
        if isinstance(countryIn, basestring):
            return fbs.filter(ForeignBorn.codeIn == countryIn).all()
        elif isinstance(countryIn, Country):
            return fbs.filter(ForeignBorn.countryIn == countryIn).all()
        else:
            raise TypeError("countryIn must be Country or String")
        
    def getFromIn(self, countryFrom, countryIn):
        fbs = self.session.query(ForeignBorn)
        if isinstance(countryFrom, basestring):
            fbs = fbs.filter(ForeignBorn.codeFrom == countryFrom)
        elif isinstance(countryFrom, Country):
            fbs = fbs.filter(ForeignBorn.countryFrom == countryFrom)
        else:
            raise TypeError("countryFrom must be Country or String")
        
        if isinstance(countryIn, basestring):
            fbs = fbs.filter(ForeignBorn.codeIn == countryIn)
        elif isinstance(countryIn, Country):
            fbs = fbs.filter(ForeignBorn.countryIn == countryIn)
        else:
            raise TypeError("countryIn must be Country or String")
            
        return fbs.all()
        
        
    def update(self, foreignBorn):
        return
    