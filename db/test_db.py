#!/usr/bin/env python

import MySQLdb

from sqlalchemy import create_engine
#from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from models import Country, ForeignBorn
from nose.tools import raises

from managers import SqlAlchemyDB

params = {
      'host': '0.0.0.0'
    , 'user': 'lukasvrabel'
    , 'passwd': ''
    , 'db': 'test'
}


mf = SqlAlchemyDB(**params)
mf.init_test_data()


        
def assert_is_country(c):
    assert hasattr(c, 'code')
    assert hasattr(c, 'name')


def assert_fbs(fbs):
    assert len(fbs)>0
    for fb in fbs:
        assert_is_fb(fb)

def assert_is_fb(fb):
    assert hasattr(fb, 'codeFrom')
    assert hasattr(fb, 'codeIn')
    assert hasattr(fb, 'count')
    assert hasattr(fb, 'countryFrom')
    assert hasattr(fb, 'countryIn')
        


def test_mysql_standard():
    db = MySQLdb.connect(**params)
    cur = db.cursor() 
    cur.execute('SELECT * FROM countries')
    
    countries = cur.fetchall()
    assert len(countries) > 0
    

        
def test_mysql_alchemy():
    url = 'mysql://{user}:{passwd}@{host}/{db}'.format(**params)
    engine = create_engine(url)

    Session = sessionmaker(bind=engine)
    session = Session()
    countries = session.query(Country).all()
    
    assert len(countries) > 0
    for c in countries:
        assert_is_country(c)

    
    
    
    
def test_cm_all():
    cm = mf.getCountryManager()
    
    countries = cm.getAll()
    assert len(countries) > 0
    for c in countries:
        assert_is_country(c)

def test_cm_get():
    cm = mf.getCountryManager()
    code = 'USA'
    
    country = cm.getByCode(code)
    #print country
    assert_is_country(country)
    assert country.code == code
    
@raises(Exception)
def test_cm_get_exception():
    cm = mf.getCountryManager()
    code = 'FOO'
    
    country = cm.getByCode(code)
    #print country
    assert_is_country(country)
    assert country.code == code
    
    
    
    
    
def test_fbm_get_all():
    fbm = mf.getForeignBornManager()

    all = fbm.getAll()
    print all
    assert len(all) > 0
    for fb in all:
        assert_is_fb(fb)
        
def test_fbm_get_from_str():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getByCountryFrom('AUS')
    print fbs
    assert_fbs(fbs)
        
def test_fbm_get_from_country():
    cm = mf.getCountryManager()
    fbm = mf.getForeignBornManager()
    country = cm.getByCode('USA')
    fbs = fbm.getByCountryFrom(country)
    print fbs
    assert_fbs(fbs)
    
@raises(TypeError)
def test_fbm_get_from_int():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getByCountryFrom(1)
    print fbs
    assert_fbs(fbs)
    
def test_fbm_get_from_nonexistent():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getByCountryFrom('SVK')
    print fbs
    assert len(fbs) <= 0
    
    
    
    
def test_fbm_get_in_str():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getByCountryIn('AUS')
    print fbs
    assert_fbs(fbs)
        
def test_fbm_get_in_country():
    cm = mf.getCountryManager()
    fbm = mf.getForeignBornManager()
    country = cm.getByCode('USA')
    fbs = fbm.getByCountryIn(country)
    print fbs
    assert_fbs(fbs)
    
@raises(TypeError)
def test_fbm_get_in_int():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getByCountryIn(1)
    print fbs
    assert_fbs(fbs)
    
def test_fbm_get_in_nonexistent():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getByCountryIn('FOO')
    print fbs
    assert len(fbs) <= 0

def test_fbm_get_from_in_str():
    fbm = mf.getForeignBornManager()
    fbs = fbm.getFromIn('AUS', 'USA')
    print fbs
    assert_fbs(fbs)
        
def test_fbm_get_from_in_country():
    cm = mf.getCountryManager()
    fbm = mf.getForeignBornManager()
    countryFrom = cm.getByCode('USA')
    countryIn = cm.getByCode('AUS')
    
    fbs = fbm.getFromIn(countryFrom, countryIn)
    print fbs
    assert_fbs(fbs)
    
@raises(TypeError)
def test_fbm_get_from_in_int():
    fbm = mf.getForeignBornManager()
    
    fbs = fbm.getFromIn(1, 2)
    print fbs
    assert_fbs(fbs)
    
def test_fbm_get_from_in_nonexistent():
    fbm = mf.getForeignBornManager()
    
    fbs = fbm.getFromIn('FOO', 'USA')
    print fbs
    assert len(fbs) <= 0
    

if __name__ == '__main__':
    test_mysql_standard()
    test_mysql_alchemy()
    
    test_cm_all()
    test_cm_get()
    test_cm_get_exception()
    
    test_fbm_get_all()
    test_fbm_get_from_str()
    test_fbm_get_from_country()
    test_fbm_get_from_int()
    test_fbm_get_from_nonexistent()
    test_fbm_get_in_str()
    test_fbm_get_in_country()
    test_fbm_get_in_int()
    test_fbm_get_in_nonexistent()
    test_fbm_get_from_in_str()
    test_fbm_get_from_in_country()
    test_fbm_get_from_in_int()
    test_fbm_get_from_in_nonexistent()
    
    


