from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref    
    
Base = declarative_base()

class Country(Base):
    __tablename__ = 'countries'
    
    code = Column(String(3), primary_key=True)
    name = Column(String(50))
    
    
    
    def __repr__(self):
        return '{} {}'.format(self.name, self.code)
    
class ForeignBorn(Base):
    __tablename__ = 'foreignBorn'
    
    codeIn = Column(String(3), ForeignKey('countries.code'), primary_key=True)
    codeFrom = Column(String(3), ForeignKey('countries.code'), primary_key=True)
    count = Column(Integer)
    
    countryIn =  relationship("Country", foreign_keys=[codeIn])
    countryFrom =  relationship("Country", foreign_keys=[codeFrom])
    
    def __repr__(self):
        return '{} from {} in {}'.format(self.count, self.codeFrom, self.codeIn)
    