CREATE TABLE IF NOT EXISTS countries (
      id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY
    , name VARCHAR(50) NOT NULL
    , code CHAR(3) NOT NULL
);


CREATE TABLE IF NOT EXISTS foreignBorn (
      idIn INT(3) NOT NULL
    , idFrom INT(3) NOT NULL
    , count INT
    , PRIMARY KEY (idIn, idFrom)
);

INSERT INTO countries(name, code) VALUES ("Australia", "AUS");
INSERT INTO countries(name, code) VALUES ("Austria", "AUT");
INSERT INTO countries(name, code) VALUES ("Belgium", "BEL");
INSERT INTO countries(name, code) VALUES ("Canada", "CAN");
INSERT INTO countries(name, code) VALUES ("Chile", "CHL");
INSERT INTO countries(name, code) VALUES ("Czech", "CZE");
INSERT INTO countries(name, code) VALUES ("Denmark", "DNK");
INSERT INTO countries(name, code) VALUES ("Estonia", "EST");
INSERT INTO countries(name, code) VALUES ("Finland", "FIN");
INSERT INTO countries(name, code) VALUES ("France", "FRA");
INSERT INTO countries(name, code) VALUES ("Germany", "DEU");
INSERT INTO countries(name, code) VALUES ("Greece", "GRC");
INSERT INTO countries(name, code) VALUES ("Hungary", "HUN");
INSERT INTO countries(name, code) VALUES ("Iceland", "ISL");
INSERT INTO countries(name, code) VALUES ("Ireland", "IRL");
INSERT INTO countries(name, code) VALUES ("Israel", "ISR");
INSERT INTO countries(name, code) VALUES ("Italy", "ITA");
INSERT INTO countries(name, code) VALUES ("Japan", "JPN");
INSERT INTO countries(name, code) VALUES ("South", "KOR");
INSERT INTO countries(name, code) VALUES ("Luxembourg", "LUX");
INSERT INTO countries(name, code) VALUES ("Mexico", "MEX");
INSERT INTO countries(name, code) VALUES ("Netherlands", "NLD");
INSERT INTO countries(name, code) VALUES ("New", "NZL");
INSERT INTO countries(name, code) VALUES ("Norway", "NOR");
INSERT INTO countries(name, code) VALUES ("Poland", "POL");
INSERT INTO countries(name, code) VALUES ("Portugal", "PRT");
INSERT INTO countries(name, code) VALUES ("Slovakia", "SVK");
INSERT INTO countries(name, code) VALUES ("Slovenia", "SVN");
INSERT INTO countries(name, code) VALUES ("Spain", "ESP");
INSERT INTO countries(name, code) VALUES ("Sweden", "SWE");
INSERT INTO countries(name, code) VALUES ("Switzerland", "CHE");
INSERT INTO countries(name, code) VALUES ("Turkey", "TUR");
INSERT INTO countries(name, code) VALUES ("United Kingdom", "GBR");
INSERT INTO countries(name, code) VALUES ("United States", "USA");
